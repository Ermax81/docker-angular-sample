# docker-angular-sample

Docker container to deploy an angular app.

I will use an example to do it.

Bezkoder has developed an angular app with pagination. Thanks!
You can find his tutorial here:
https://bezkoder.com/angular-10-pagination-ngx/

The source code is available here: 
https://github.com/bezkoder/angular-10-pagination-example

Copy Dockerfile in the project.

Commands:

	git clone https://github.com/bezkoder/angular-10-pagination-example.git
	cp Dockerfile angular-10-pagination-example/
	cd angular-10-pagination-example
	docker build -t angular-sample .
	docker run -tid -p 4200:4200 --name angular-sample angular-sample
	
	
To follow log file: docker logs -f angular-sample
When you have "Compiled successfully", open your browser at http://localhost:4200

To stop the container: docker stop angular-sample

To remove the container: docker rm -f angular-sample

To remove angular-sample image: docker rmi angular-sample

